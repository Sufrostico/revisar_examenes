# Archivo de configuración para revisión del examen

El archivo de configuración para revisión del examen contiene varias secciones
que se detallaran a continuación:

## Código

Es el nombre en código de la evaluación

```conf
	Codigo: examen01
```

## Puntaje

Esta sección es una lista de elementos donde cada elemento representa el puntaje
asignado a cada ejercicio por orden de aparición

```conf
	Puntaje:
		- 0.25
		- 0.25
		- 0.25
		- 0.25
```

## Pruebas 

En esta sección se colocan las pruebas que se van a realizar por ejercicio. Cada
prueba es una lista con elementos en el siguiente orden:

~~~
	[ Identificador de la prueba,
	  Nombre de la función a probar, 
	  Lista con argumentos de la función,
	  Valor esperado de la respuesta ]
~~~

---

Un ejemplo completo de pruebas para cuatro ejercicios puede ser encontrado a
continuación:

``` conf
	Pruebas:

		E01:
			- [0, "resta_pares", [123456], 22446]
			- [1, "resta_pares", [23450304], 22440204 ]
			- [2, "resta_pares", [38200726], 28200626 ]
			- [3, "resta_pares", [4063], "Error03" ]
			- [4, "resta_pares", [-123456], "Error02"]
			- [5, "resta_pares", ["9870654"], "Error01"]
			- [6, "resta_pares", [654321], 644220 ]
			- [7, "resta_pares", [875578], 864468 ]
			- [8, "resta_pares", [9876.54], "Error01"]
			- [9, "resta_pares", [98765], "Error03"]

		E02:
			- [0, "cuenta_multiplos", [12345, 2], 2]
			- [1, "cuenta_multiplos", [12345, 3], 1]
			- [2, "cuenta_multiplos", [12345, 5], 1]
			- [3, "cuenta_multiplos", [123456789, 4], 2]
			- [4, "cuenta_multiplos", [123456, -3], "Error02"]
			- [5, "cuenta_multiplos", [9650895, "4"], "Error01"]
			- [6, "cuenta_multiplos", [9650.895, 4], "Error01" ]
			- [7, "cuenta_multiplos", [-123456, 7], "Error02" ]
			- [8, "cuenta_multiplos", [123456789, 3], 3]
			- [9, "cuenta_multiplos", [[9650895], 4], "Error01"]

		E03:
			- [0, "fibonacci", [0], 0]
			- [1, "fibonacci", [1], 1]
			- [2, "fibonacci", [2], 1]
			- [3, "fibonacci", [3], 2]
			- [4, "fibonacci", ["3"], "Error01"]
			- [5, "fibonacci", [6], 8]
			- [6, "fibonacci", [-3], "Error01" ]
			- [7, "fibonacci", [3.3], "Error01"]
			- [8, "fibonacci", [7], 13]
			- [9, "fibonacci", [5], 5]
		
		E04:
			- [0, "collatz", [1], 1]
			- [1, "collatz", ["1"], "Error01"]
			- [2, "collatz", [-7], "Error02"]
			- [3, "collatz", [2], 2]
			- [4, "collatz", [3], 47]
			- [5, "collatz", [4], 4]
			- [6, "collatz", [5], 32]
			- [7, "collatz", [[1]], "Error01"]
			- [8, "collatz", [-70], "Error02"]
			- [9, "collatz", [6], 50]

```

