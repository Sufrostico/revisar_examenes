def resta_pares(numero):

    if type(numero) != int:
        return "Error01"

    elif numero < 0:
        return "Error02"

    else:
        return resta_pares_aux(numero, 1)

def resta_pares_aux(numero, factor):

    if numero == 0:
        return 0

    elif (numero%10) % 2 == 0:
        return ((numero%10) - 1) * factor + resta_pares_aux(numero//10, factor*10)

    else:
        return (numero%10) * factor + resta_pares_aux(numero//10, factor*10)


# --- --- --- #

def contar(numero):
    """
    Esta es la función para contar
    """

    if not isinstance(numero, int):
        return "Error01"
    else:
        return contar_aux(numero, 0)

def contar_aux(numero, resultado):
    """
    Docstring
    """
    if numero == 0:
        return resultado

    else:
        return contar_aux(numero//10, resultado +1)

