#!/usr/bin/python3

'''
    Archivo con la plantilla para casos de pruebas
'''
import unittest

#http://eli.thegreenplace.net/2011/08/02/python-unit-testing-parametrized-test-cases/
class ParametrizedTestCase(unittest.TestCase):
    """ TestCase classes that want to be parametrized should
        inherit from this class.
    """
    def __init__(self, methodName='runTest', param=None, examen=None):
        super(ParametrizedTestCase, self).__init__(methodName)
        self.param = param
        self.calificando = examen

    @staticmethod
    def parametrize(testcase_klass, param=None, examen=None):
        """ Create a suite containing all tests taken from the given
            subclass, passing them the parameter 'param'.
        """
        testloader = unittest.TestLoader()

        testnames = testloader.getTestCaseNames(testcase_klass)
        suite = unittest.TestSuite()

        for name in testnames:
            suite.addTest(testcase_klass(name, param=param, examen=examen))

        return suite


# Test Default ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class PruebasEjercicio(ParametrizedTestCase):
    '''
    Plantilla de plan de pruebas.
    '''

    calificando = None

    error01 = ['E01', 'Error1', 'error1', 'error01', 'Err01', 'Error01.', 'Error01', 'Error 01',
               "'Error01.'", "'Error01'", ' Error01.', 'error 01', '(Error01)', 'ERROR01',]

    error02 = ['E02', 'Error2', 'error2', 'error02', 'Err02', 'Error02.', 'Error02', 'Error 02',
               "'Error02.'", "'Error02,'", "Error02.'", 'error 02', '(Error02)', 'ERROR02',]

    error03 = ['E03', 'Error3', 'error3', 'error03', 'Err03', 'Error03.', 'Error03', 'Error 03',
               "'Error03.'", "'Error03,'", "Error03.'", 'error 03', '(Error03)', 'ERROR03',]

    error04 = ['E04', 'Error4', 'error4', 'error04', 'Err04', 'Error04.', 'Error04', 'Error 04',
               "'Error04.'", "'Error04,'", "Error04.'", 'error 04', '(Error04)', 'ERROR04',]

    def test(self):
        '''
        Caso de prueba plantilla
        '''

        #funcion, argumentos, resultado_esperado
        nombre_funcion = self.param[1]
        argumentos = self.param[2]
        resultado_esperado = self.param[3]

        funcion = getattr(self.calificando, nombre_funcion)
        resultado = funcion(*argumentos)

        identificador_test = "[Test ID: " + str(self.param[0]) + "] "+nombre_funcion
        with self.subTest(msg=identificador_test, argumentos=argumentos):

            if resultado is None:
                self.fail('La funcion retorna: None')

            if resultado_esperado == "Error01":
                self.assertIn(resultado, self.error01)

            elif resultado_esperado == "Error02":
                self.assertIn(resultado, self.error02)

            elif resultado_esperado == "Error03":
                self.assertIn(resultado, self.error03)

            elif resultado_esperado == "Error04":
                self.assertIn(resultado, self.error04)

            else:
                self.assertEqual(resultado, resultado_esperado)

#**********************************************************************#
