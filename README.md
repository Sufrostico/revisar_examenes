# Aplicación para revisar exámenes


## Instalación

```bash
	$ pip3 install git+https://gitlab.com/Sufrostico/revisar_examenes.git
```

## Para revisarlos todos al mismo tiempo

Todas las pruebas a revisar deben estar en una carpeta recibidos

~~~
revisar_examenes --guardar --configfile examen01.conf
~~~


## Archivo de configuración

Con este repositorio se provee un archivo de configuración en la carpeta
**examples** e información de como entenderlo en el archivo README.md de 
la carpeta example.
