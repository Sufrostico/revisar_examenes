#!/usr/bin/env python3	

from setuptools import setup	

setup(name='revisarExamenes',
      version='1.0',
      description='Herramienta para asistir en la revisión de exámenes de introducción a la programación',	
      author='Jaime Gutiérrez, Aurelio Sanabria',	
      author_email='jgutierrez@itcr.ac.cr, ausanabria@itcr.ac.cr',
      url='www.github.com/Sufrostico/revisarExamenes',	
      packages=['biblioteca_pruebas',],
      package_data={'biblioteca_pruebas': ['biblioteca_pruebas/*'] },	
      scripts=['scripts/revisar_examenes'],								
      install_requires=['pathlib', 'pyyaml'],						
      classifiers=[
      		'Development Status :: 4 - Beta',
      		'Environment :: Console',
     		'Intended Audience :: End Users/Desktop',
      		'License :: OSI Approved :: GPL 3.0',
      		'Operating System :: POSIX',
      		'Programming Language :: Python',
      		'Topic :: Software Development :: Bug Tracking',
      		],
)

